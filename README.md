# Cvičení 6

Dnes jsme se rozhodli si vás vyzkoušet na úlohách ze cvičennice, a to protože
se nám moc líbily (a možná protože jsem trochu líný se psát s něčím lepším, když už jsou to takové nádhery 😉).

Úlohy jsou až na pár rozdílů totožné jako v ní, a ty rozdíly jsou pouze pro to, aby
se výsledky daly snáze testovat.

## Úlohy

### 1.
Vytvořte funkci SortWithoutDuplicates, která přijímá jako parametr vector integerů. 
Tato metoda má za úkol zbavit tento vector duplicitních hodnot 
(nechat pouze jeden výskyt každé hodnoty). 
Následně neduplicitní hodnoty vraťte ve vektoru v zestupném pořadí.

V souboru [practice.cpp](practice.cpp) je pro to připravená funkce `SortWithoutDuplicates`.

### 2.
Implementujte třídu CPrefixCounter. Třída bude mít metodu AddWord, 
metoda bude mít jediný parametr typu string a postará se o přidání 
slova do slovníku. Pokud slovo ve slovníku existuje, nebude přidáno. 
Dále bude mít metodu CountPrefix, která přijímá 
také parametr typu string a vrátí počet slov ze slovníku, 
která začínají daným řetězcem.

V souboru [practice.cpp](practice.cpp) je pro to připravená třída `CPrefixCounter`.

### 3.
Vytvořte funktor FHashString. Funktor má za úkol provést výpočet hashe 
zadaného řetězce. Hash bude odpovídat součtu ASCII hodnot znaků v řetězci.

V souboru [practice.cpp](practice.cpp) je pro to připravený funktor `FHashString`.

### 4.
Realizujte funktor FComplicatedWords, který bude přijímat jako parametr větu (string). 
Funktor má za úkol spočítat počet komplikovaných slov ve větě (můžete 
se spolehnout, že jednotlivá slova jsou oddělená bílými znaky). 
Komplikované slovo je definované jako slovo, které obsahuje minimálně 
4 samohlásky.

V souboru [practice.cpp](practice.cpp) je pro to připravený funktor `FComplicatedWords`.

## Bonusové úlohy
Za splnění každé z nich je možné do konce cvičení získat 1 bod navíc.

### 5. 
Implementujte funkci SortByCount, která dostane na vstupu vector čísel. Na výstupu 
vraťte vector čísel, který bude mít přeuspořádané hodnoty tak, že čísla 
budou seřazená podle počtu jejich výskytů, pokud se nějaká čísla vyskytují 
stejněkrát, tak je seřaďte podle jejich hodnot. Např. pro vstup 
{1, 4, 1, 7, 4, 5, 7, 7} bude odpovídající výstup {7, 7, 7, 1, 1, 4, 4, 5}, 
protože číslo 7 se ve vstupním vectoru vyskytovalo nejvícekrát, 
číslo 1 se vystuje stejněkrát jako 4, ale má nižší hodnotu a číslo 5 se 
vyskytuje pouze jednou.

V souboru [practice.cpp](practice.cpp) je pro to připravená funkce `SortByCount`.

### 6.
Vytvořte funktor FCompress, který zkomprimuje rozsah čísel. Vaším úkolem je pro 
zadanou posloupnost čísel transformovat pole do podoby, že obsahuje pouze 
čísla v rozsahu <1;K> (K je počet různých hodnot ve vstupní posloupnosti) 
a kde je dodrženo, že pokud bylo číslo na indexu i vyšší, než číslo na 
indexu j ve vstupní posloupnosti, tak tomu tak bude i v té výstupní. 
Analogicky, kde byly stejné hodnoty na vstupu, tak musí být stejné i na 
výstupu. Např. pro pole {1, 17, 2000, 13, 1, 2} by byl odpovídající 
výstup {1, 4, 5, 3, 1, 2}.

V souboru [practice.cpp](practice.cpp) je pro to připravený funktor  `FCompress`.
