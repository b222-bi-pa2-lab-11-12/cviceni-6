#define __AUTOMATIC_TESTS__

#include "../practice.cpp"
#include <cassert>

int main() {
    vector<int> i;
    assert(SortWithoutDuplicates(i).empty());
    i = {1,1,1,1};
    assert(SortWithoutDuplicates(i) == (vector<int> {1}));
    i = {1,3,2,4};
    assert(SortWithoutDuplicates(i) == (vector<int> {4,3,2,1}));
    i = {1,3,2,4,1,2,4,1,1,6,8,7,6,10,-40,8,8,6,8};
    assert(SortWithoutDuplicates(i) == (vector<int> {10,8,7,6,4,3,2,1,-40}));
    cout << "Uspesne splneno" << endl;
    return EXIT_SUCCESS;
}