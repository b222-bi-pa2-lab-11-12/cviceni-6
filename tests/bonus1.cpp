#define __AUTOMATIC_TESTS__

#include "../practice.cpp"
#include <cassert>

int main() {
    vector<int> i;
    assert(SortByCount(i).empty());
    i = {1,1,1,1};
    assert(SortByCount(i) == (vector<int> {1,1,1,1}));
    i = {1,3,2,4};
    assert(SortByCount(i) == (vector<int> {1,2,3,4}));
    i = {1,3,2,4,1,2,4,1,1,6,8,7,6,10,-40,8,8,6,8};
    assert(SortByCount(i) == (vector<int> {1,1,1,1,8,8,8,8,6,6,6,2,2,4,4,-40,3,7,10}));
    i = {1, 4, 1, 7, 4, 5, 7, 7};
    assert(SortByCount(i) == (vector<int> {7, 7, 7, 1, 1, 4, 4, 5}));
    cout << "Uspesne splneno" << endl;
    return EXIT_SUCCESS;
}