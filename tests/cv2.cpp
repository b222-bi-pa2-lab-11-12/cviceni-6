#define __AUTOMATIC_TESTS__

#include "../practice.cpp"
#include <cassert>

int main() {
    CPrefixCounter x;
    assert(x.CountPrefix("aaa") == 0);
    assert(x.CountPrefix("") == 0);
    assert(x.AddWord("aaaaa").CountPrefix("a") == 1);
    assert(x.AddWord("aaaaa").CountPrefix("b") == 0);
    assert(x.AddWord("aaaaa").CountPrefix("") == 1);
    assert(x.AddWord("aaaaa").CountPrefix("aaaaa") == 1);
    assert(x.AddWord("aaaaa").CountPrefix("aaaaaa") == 0);
    assert(x.AddWord("aaaaa").CountPrefix("baaaaa") == 0);
    assert(
        x
        .AddWord("a")
        .AddWord("aa")
        .AddWord("aaa")
        .AddWord("aaaa")
        .AddWord("aaaaa")
    .CountPrefix("b") == 0);
    assert(x.CountPrefix("a") == 5);

    assert(x
            .AddWord("aedsfsdf")
            .AddWord("asdfasdf")
            .AddWord("sdfdf")
            .AddWord("sdfsd")
            .AddWord("asxfasdf")
            .CountPrefix("a") == 8);
    assert(x.CountPrefix("s") == 2);
    assert(x.CountPrefix("sd") == 2);
    assert(x.CountPrefix("sdf") == 2);
    assert(x.CountPrefix("sdfs") == 1);
    assert(x.CountPrefix("sdfsd") == 1);
    assert(x.CountPrefix("sdfsdx") == 0);
    assert(x.CountPrefix("sdfd") == 1);
    assert(x.CountPrefix("sdfdf") == 1);
    assert(x.CountPrefix("sdfdfx") == 0);
    assert(x.CountPrefix("sdfx") == 0);
    cout << "Uspesne splneno" << endl;
    return EXIT_SUCCESS;
}