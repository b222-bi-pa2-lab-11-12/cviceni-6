#define __AUTOMATIC_TESTS__

#include "../practice.cpp"
#include <cassert>

int main() {
    FComplicatedWords n;
    assert(n("") == 0);
    assert(n("reee") == 0);
    assert(n("reeeee") == 1);
    assert(n("reeeeeeeeeeeee") == 1);
    assert(n("reeeeeeeeeeeee    re") == 1);
    assert(n("reeeeeeeeeeeee re") == 1);
    assert(n("reeeeeeeeeeeee reeee") == 2);
    assert(n("auie") == 1);
    assert(n("ayoo") == 1);
    assert(n("ayo") == 0);
    assert(n("ayo ayo ayo ayo ayo ayo ") == 0);
    assert(n("ayo ayo ayoo ayo ayo ayoo ") == 2);
    assert(n("WHAT AM I DOING WITH MY LIFE?!?!?!?!?!?") == 0);
    assert(n("\"'?.,!+-$*/\\") == 0);
    cout << "Uspesne splneno" << endl;
    return EXIT_SUCCESS;
}