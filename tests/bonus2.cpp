#define __AUTOMATIC_TESTS__

#include "../practice.cpp"
#include <cassert>

template<class T>
void printVec(const T & a) {
    for(auto & i: a) {
        cout << i << ",";
    }
    cout << endl;
}

int main() {
    vector<int> i;
    FCompress f;
    assert(f(i).empty());
    i = {1,1,1,1};
    assert(f(i) == (vector<int> {1,1,1,1}));
    i = {1,3,2,4};
    assert(f(i) == (vector<int> {1,3,2,4}));
    i = {1,3,2,4,1,2,4,1,1,6,8,7,6,10,-40,8,8,6,8};
    assert(f(i) == (vector<int> {2,4,3,5,2,3,5,2,2,6,8,7,6,9,1,8,8,6,8}));
    i =  {1, 17, 2000, 13, 1, 2};
    assert(f(i) == (vector<int> {1, 4, 5, 3, 1, 2}));
    cout << "Uspesne splneno" << endl;
    return EXIT_SUCCESS;
}