#define __AUTOMATIC_TESTS__

#include "../practice.cpp"
#include <cassert>

int main() {
    FHashString n;
    assert(n("") == 0);
    assert(n("reeeeeeeeeeeee") == 1427);
    assert(n("WHAT AM I DOING WITH MY LIFE?!?!?!?!?!?") == 2397);
    assert(n("\"'?.,!+-$*/\\") == 564);
    cout << "Uspesne splneno" << endl;
    return EXIT_SUCCESS;
}